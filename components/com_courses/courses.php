<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Courses
 * @author     Rob Tyson <tania@thefarmfactory.co.uk>
 * @copyright  2018 Rob Tyson
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Courses', JPATH_COMPONENT);
JLoader::register('CoursesController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Courses');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
