<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Courses
 * @author     Tania Boyd <tania@thefarmfactory.co.uk>
 * @copyright  2018 Rob Tyson
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JFactory::getApplication()->getPathWay();
?>      
        <div class="item-page" itemscope="" itemtype="https://schema.org/Article">
            <meta itemprop="inLanguage" content="en-GB">

            <div class="page-header">
                <h2 itemprop="headline">
                    <?php echo $this->item->course_name; ?> 
                </h2>
                <div itemprop="articleBody">
                </div>
              
            </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-grid">                        

            <div class="g-block size-100 course-introduction">
                <div class="g-content">
                    <div class="platform-content">
                        <div class="moduletable ">
                            <h3 class="g-title"><?php echo $this->item->tagline; ?></h3>	
						

                            <div class="custom">
                                <?php echo $this->item->description; ?>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        </div>  
       
        <div class="g-grid">    
            <div class="g-block size-70 course-images">
                <div class="g-content">
                    <div class="moduletable ">
                        <div class="course-image"> 
                        <?php
			foreach ((array) $this->item->image as $singleFile) : 
                            if (!is_array($singleFile)) : 
                                    $uploadPath = 'images/course-images' . DIRECTORY_SEPARATOR . $singleFile;
                                    echo '<image src=' . JRoute::_(JUri::root() . $uploadPath, false) . '>';
                            endif;
			endforeach;
                        ?>
                        </div>		
                    </div>
            
                </div>
            </div>
            <div class="g-block size-30">
                <div class="g-content">
                    <div class="moduletable ">
                        <div class="course-details"> 
                            <h4> <?php echo $this->item->details_title; ?> </h4>
                            <p><?php echo $this->item->details_description; ?></p>
                            <table border="0" class="course-info">                                
                                <tbody>
                                    <?php if ($this->item->course_length) { ?>
                                    <tr>                              
                                            <td><?php echo JText::_('COM_COURSES_FORM_LBL_COURSE_COURSE_LENGTH'); ?></td>
                                            <td><?php echo $this->item->course_length; ?></td>
                                        
                                    </tr>
                                    <?php } ?>
                                    <?php if ($this->item->hours_per_week) { ?>
                                    <tr>                                        
                                        <td><?php echo 'Hours Per Week/Day'; ?></td>
                                        <td><?php echo $this->item->hours_per_week; ?></td>
                                       
                                    </tr>
                                     <?php } ?>
                                    <?php if ($this->item->time) { ?>
                                    <tr>
                                        <td><?php echo JText::_('COM_COURSES_FORM_LBL_COURSE_TIME'); ?></td>
                                        <td><?php echo $this->item->time; ?></td>
                                    </tr>
                                    <?php } ?>
                                    <?php if ($this->item->average_group_size) { ?>
                                    <tr>
                                        <td><?php echo JText::_('COM_COURSES_FORM_LBL_COURSE_AVERAGE_GROUP_SIZE'); ?></td>
                                        <td><?php echo $this->item->average_group_size; ?></td>
                                    </tr>
                                    <?php } ?>
                               
                                </tbody>
                                
                            </table>                           
                                        <?php if ($this->item->buynowbtn) { ?>
                                 <?php echo $this->item->buynowbtn; ?>
                                    <?php } ?>
                               
                        </div>		
                    </div>
            
                </div>
            </div>
        </div>  
        <?php if (( $this->item->course_aims ) || ( $this->item->course_aims ))  { ?>
        <div class="g-grid">   
            <?php if (( $this->item->course_aims )) { ?>
            <div class="g-block size-70">
                <div class="g-content">
                    <div class="platform-content">
                        <div class="moduletable ">		
                          
                                <div class="custom">
                                   <?php echo $this->item->course_aims; ?>
                                </div>
                            
                        </div>
                    </div>
                </div>            
            </div>
            <?php } ?>
            <?php if (( $this->item->course_aims )) { ?>
            <div class="g-block size-30">
                <div class="g-content">
                    <div class="platform-content">
                        <div class="moduletable ">                   
                            <div class="custom">
                                <?php echo $this->item->tuition_fees; ?>
                            </div>                           
                        </div>
                    </div>
                </div>            
            </div>
             <?php } ?>
        </div> 
        <?php } ?>
        
          <?php if ( isset($this->item->course_aims)) {?>
          <div class="g-grid"> 
            <div class="g-block size-100">
                <div class="g-content">
                    <div class="platform-content">
                        <div class="moduletable ">		
                          
                                <div class="custom timetable">
                                   <?php echo $this->item->course_timetable;?>                                  
                                </div>
                            
                        </div>
                    </div>
                </div>            
            </div>
        </div>
        
        <?php } ?>


