<?php


defined('_JEXEC') or die;

// Include the news functions only once
JLoader::register('coursePage', __DIR__ . '/helper.php');


if(!isset($_GET['_id'])) {
    $vac = false;
}else{
    $vac = coursePage::getCourse($_GET['_id']);
}

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'), ENT_COMPAT, 'UTF-8');
require JModuleHelper::getLayoutPath('mod_courses', $params->get('layout', 'default'));
