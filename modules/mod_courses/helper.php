<?php

defined('_JEXEC') or die;


class coursePage
{

    public static function getCourse($i)
    {
        //CLEAN ID
        $i = preg_replace('/[^0-9]/', '', $i);
        
        if($i == '')
            return false;

        //GRAB DB
        $db = Jfactory::getDbo();

        //WRITE QUERY
		
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName('#__courses', 'courses'));
	
		$query->where($db->quoteName('courses.id'). ' = ' . $db->quote($i));

        //SET QUERY THE START OF THE QUERY
        $db->setQuery($query);

        //GET VACANCIES
        $event = $db->loadObjectList();

        //CHECK IF EMPTY
        if(empty($event) || !$event)
            return false;

        return $event[0];
    }
}
