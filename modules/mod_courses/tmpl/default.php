<?php


defined('_JEXEC') or die;

?>
<div class="main-events holder <?php echo $moduleclass_sfx; ?>">
     
    <?php if($vac):?>
            <div class="event">
                <h2 class="date-title">
                    <span>
                    <?php 
                        $date = new datetime($vac->date);                
                        echo '<strong>'.$date->format('d /m/Y').' - '.  $vac->opening_start_time.'</strong>'; ?>
                    </span>			
                </h2>
                <div class="event-title">
                    <h2><?php echo $vac->title;?></h2>                    
                </div>
                <div class="split">
                    <div>
                        <img class="event-img" src="<?php echo $vac->image; ?>" alt="">
                    </div>
                    
                    <div class="location-content">
                        <h3>Location</h3>
                        <p> 
                            <?php echo $vac->venue ?> <br/>
                            <?php echo $vac->address ?> <br/>
                            
                        </p>
                      
                        <h3>Price</h3>                        
                        <p> 
                            <?php echo $vac->ticket_price ?> 
                        </p>
                         <h3>Includes</h3>  
                        <p>
                        <?php echo $vac->includes ?>
                        </p>
                    
                        <?php if($vac->eventlink) { ?>
                            <a class="kpz-norm-btn-pink" target="_blank" href="<?php echo $vac->eventlink ?>">More Information</a>
                        <?php } ?>
                
                    </div>
                </div>
                
                <div class="event-content">
                    <?php
                        $description = $vac->description;                            
                        echo '<p>'. $description .'</p>';                            
                    ?>
                </div>
                
            </div>
    <?php else:?>
        <h2>Sorry we couldn't find the event you was looking for.</h2>
    <?php endif;?>
          <div><a href="/events/" class="kpz-norm-btn-pink">< Back to Events</a></div>
</div>
