<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Courses
 * @author     Rob Tyson <tania@thefarmfactory.co.uk>
 * @copyright  2018 Rob Tyson
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Course controller class.
 *
 * @since  1.6
 */
class CoursesControllerCourse extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'courses';
		parent::__construct();
	}
}
