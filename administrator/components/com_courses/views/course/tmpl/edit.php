<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Courses
 * @author     Rob Tyson <tania@thefarmfactory.co.uk>
 * @copyright  2018 Rob Tyson
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_courses/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'course.cancel') {
			Joomla.submitform(task, document.getElementById('course-form'));
		}
		else {
			
			if (task != 'course.cancel' && document.formvalidator.isValid(document.id('course-form'))) {
				
				Joomla.submitform(task, document.getElementById('course-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_courses&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="course-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_COURSES_TITLE_COURSE', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<?php echo $this->form->renderField('course_name'); ?>
				<?php echo $this->form->renderField('tagline'); ?>
				<?php echo $this->form->renderField('description'); ?>
				<?php echo $this->form->renderField('image'); ?>

				<?php if (!empty($this->item->image)) : ?>
					<?php $imageFiles = array(); ?>
					<?php foreach ((array)$this->item->image as $fileSingle) : ?>
						<?php if (!is_array($fileSingle)) : ?>
							<a href="<?php echo JRoute::_(JUri::root() . '/images/course-images/' . DIRECTORY_SEPARATOR . $fileSingle, false);?>"><?php echo $fileSingle; ?></a> | 
							<?php $imageFiles[] = $fileSingle; ?>
						<?php endif; ?>
					<?php endforeach; ?>
					<input type="hidden" name="jform[image_hidden]" id="jform_image_hidden" value="<?php echo implode(',', $imageFiles); ?>" />
				<?php endif; ?>				<?php echo $this->form->renderField('details_title'); ?>
				<?php echo $this->form->renderField('details_description'); ?>
				<?php echo $this->form->renderField('course_length'); ?>
				<?php echo $this->form->renderField('hours_per_week'); ?>
				<?php echo $this->form->renderField('time'); ?>
				<?php echo $this->form->renderField('average_group_size'); ?>
				<?php echo $this->form->renderField('course_aims'); ?>
				<?php echo $this->form->renderField('tuition_fees'); ?>
				<?php echo $this->form->renderField('course_timetable'); ?>
				<?php echo $this->form->renderField('buynowbtn'); ?>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
