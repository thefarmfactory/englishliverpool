CREATE TABLE IF NOT EXISTS `#__courses` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`course_name` VARCHAR(255)  NOT NULL ,
`tagline` VARCHAR(255)  NOT NULL ,
`description` TEXT NOT NULL ,
`image` TEXT NOT NULL ,
`details_title` VARCHAR(255)  NOT NULL ,
`details_description` TEXT NOT NULL ,
`course_length` VARCHAR(255)  NOT NULL ,
`hours_per_week` VARCHAR(255)  NOT NULL ,
`time` VARCHAR(255)  NOT NULL ,
`average_group_size` VARCHAR(255)  NOT NULL ,
`course_aims` TEXT NOT NULL ,
`tuition_fees` TEXT NOT NULL ,
`course_timetable` VARCHAR(255)  NOT NULL ,
`buynowbtn` TEXT NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;

